#!/bin/sh
rm -rf ~/.config/nvim &&
rm -rf ~/.local/share/nvim &&
git clone https://github.com/NvChad/NvChad ~/.config/nvim --depth 1 && 
rm -rf ~/.config/nvim/lua/custom &&
git clone git@gitlab.com:talvdav/nvchad-custom ~/.config/nvim/lua/custom && nvim
