# dotfiles

## About Xresources vs. Xdefaults
Read [Xresources or Xdefaults](https://superuser.com/questions/243914/xresources-or-xdefaults#243916) (warning SPARKLES!!!)

If your systems wants an .Xdefaults, just make a symlink to Xresources.
