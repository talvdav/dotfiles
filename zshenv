unset LC_ALL
export LANGUAGE='en_DE:de'
export LC_ALL='en_US.UTF-8'
export LC_COLLATE='en_US.UTF-8'
export LC_CTYPE='en_US.UTF-8'
export LC_MESSAGES='en_US.UTF-8'
export LC_MONETARY='en_US.UTF-8'
export LC_NUMERIC='en_US.UTF-8'
export LC_PAPER='de_US.UTF-8'
export LC_TIME='en_US.UTF-8'
export LANG='en_US.UTF-8'
export AWT_TOOLKIT=XToolkit

OSType=$(uname -s)

case $OSType in
    Darwin)
        PATH="$PATH:/Applications/Racket v6.7/bin"
        PATH="$PATH:/usr/local/bin"
        PATH="$PATH:~/Applications/love.app/Contents/MacOS"
        BOOT_JVM_OPTIONS="-client -XX:+TieredCompilation -XX:TieredStopAtLevel=1 -Xmx2g -XX:MaxPermSize=128m -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -Xverify:none"
        export BOOT_JVM_OPTIONS
esac

export XDG_CONFIG_HOME="$HOME/.config" 

export DENO_INSTALL="/home/thomas/.deno"

CARGO_ENV="$HOME/.cargo/env"
if [[ -e $CARGO_ENV ]]; then
    source $CARGO_ENV
    export PATH=$PATH:$HOME/.cargo/bin
fi
export GOPATH="/home/thomas/.go"
export PATH=$PATH:$DENO_INSTALL/bin:$HOME/.cargo/bin:$HOME/.local/bin:$HOME/bin:$HOME/.go/bin:$HOME/.zvm/bin:$HOME/.zvm/self:$HOME/bin/flutter/bin:$HOME/bin/flutter/bin/cache/dart-sdk/
