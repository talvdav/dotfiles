# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd extendedglob
bindkey -e
bindkey "\e[3~" delete-char
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/thomas/.zshrc'

autoload -Uz compinit && compinit
# End of lines added by compinstall

zstyle ':vcs_info:*' formats '%s %b '

autoload -Uz colors && colors
autoload -Uz promptinit && promptinit

autoload -Uz vcs_info 
precmd_vcs_info() { vcs_info }
precmd_functions+=(precmd_vcs_info)
setopt prompt_subst

OSType=$(uname -s)
case $OSType in

    Darwin)
        [[ -x "/usr/local/bin/emacs" ]] && alias emacs='/usr/local/bin/emacs -nw'
        alias ls='ls -G'
        ;;

    FreeBSD)

    # keys for freebsd
    typeset -g -A key
    key[F1]='^[OP'
    key[F2]='^[OQ'
    key[F3]='^[OR'
    key[F4]='^[OS'
    key[F5]='^[[15~'
    key[F6]='^[[17~'
    key[F7]='^[[18~'
    key[F8]='^[[19~'
    key[F9]='^[[20~'
    key[F10]='^[[21~'
    key[F11]='^[[23~'
    key[F12]='^[[24~'
    key[Backspace]='^?'
    key[Insert]='^[[2~'
    key[Home]='^[[H'
    key[PageUp]='^[[5~'
    key[Delete]='^[[3~'
    key[End]='^[[F'
    key[PageDown]='^[[6~'
    key[Up]='^[[A'
    key[Left]='^[[D'
    key[Down]='^[[B'
    key[Right]='^[[C'
    key[Menu]=''

    # fix lxterm
    key[Home_]='^[OH'
    key[End_]='^[OF'

    # setup key accordingly
    [[ -n "${key[Home]}"    ]]  && bindkey  "${key[Home]}"    beginning-of-line
    [[ -n "${key[End]}"     ]]  && bindkey  "${key[End]}"     end-of-line
    [[ -n "${key[Insert]}"  ]]  && bindkey  "${key[Insert]}"  overwrite-mode
    [[ -n "${key[Delete]}"  ]]  && bindkey  "${key[Delete]}"  delete-char
    #[[ -n "${key[Up]}"      ]]  && bindkey  "${key[Up]}"      up-line-or-history
    #[[ -n "${key[Down]}"    ]]  && bindkey  "${key[Down]}"    down-line-or-history
    #[[ -n "${key[Left]}"    ]]  && bindkey  "${key[Left]}"    backward-char
    #[[ -n "${key[Right]}"   ]]  && bindkey  "${key[Right]}"   forward-char

    # fix lxterm
    [[ -n "${key[Home_]}"    ]]  && bindkey  "${key[Home_]}"    beginning-of-line
    [[ -n "${key[End_]}"     ]]  && bindkey  "${key[End_]}"     end-of-line

    alias ls='ls -G'
    ;;

*)
    alias ls='ls --color=auto'
esac

[[ -r '/etc/profile.d/nix.sh' ]] && source '/etc/profile.d/nix.sh'

setopt PROMPT_SUBST
PS1='[%{$fg[yellow]%}%n%{$reset_color%}@%{$fg[magenta]%}%m%{$reset_color%}][%{$fg[cyan]%}%~%{$reset_color%}]
[%{$fg[green]%}${vcs_info_msg_0_}%{$reset_color%}%{$fg[green]%}%%%{$reset_color%}] '

RPS1='[%{$fg[yellow]%}%D{%Y/%m/%d - %H:%M:%S}%{$reset_color%}]'


alias la='ls -a'
alias ll='ls -l'
alias lisa='ls -fucklisa'

alias zdup='sudo zypper dup --no-allow-vendor-change'
alias nixconf='sudoedit /etc/nixos/configuration.nix'
alias nixos-rebuild='sudo nixos-rebuild'
alias hx='helix'
alias lg='lazygit'

# alias ema='emacs'
alias emacs='emacs -nw'
alias Emacs='emacsclient -t -c -a emacs'
alias ecnw='emacsclient -nw -t -c -a emacs'

export ALTERNATE_EDITOR='emacs -nw'
export EDITOR='emacs -nw'
export VISUAL='emacs -nw'

echo -e '\033[?48c'
clear
