


# define mklink  
# New-Item -ItemType SymbolicLink -Path $(2) -Value $(1) 
# endef



create_link = ln -s $(1) $(2)



.PHONY: all

all:
	$(call create_link,.\zshrc, .\zshrc_link)